
SCRIPTS = init shutdown

all:

install: $(SCRIPTS)
	for script in $^ ; do\
	    install -D -m 755 "$$script" $(DESTDIR)/"$$script"; \
	done
